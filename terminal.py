from IPython.terminal.embed import InteractiveShellEmbed
from dotenv import dotenv_values

from gcs import GCS
from gcs.relays import RelayState


def main():
    config = dotenv_values('.env')
    gcs = GCS(**config)

    scope = {'gcs': gcs,
             'RelayState': RelayState}
    shell = InteractiveShellEmbed(banner1='GCS terminal', user_ns=scope)
    shell()


if __name__ == '__main__':
    main()
