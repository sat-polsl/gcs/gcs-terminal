import zmq
from typing import Optional
import json

from zmq_util import request


class Rotator:
    """
    Class for controlling rotator.
    """
    _ROTOR_CTL_TOPIC = 'rotor_ctl'

    def __init__(self, publisher: zmq.Socket, subscriber: zmq.Socket, poller: zmq.Poller, timeout: int) -> None:
        """
        Constructor.
        :param publisher: ZMQ publisher socket.
        :param subscriber: ZMQ subscriber socket.
        :param poller: ZMQ poller.
        :param timeout: Timeout in milliseconds.
        """
        self._publisher = publisher
        self._subscriber = subscriber
        self._poller = poller
        self._timeout = timeout

    def rotate(self, azimuth: Optional[float] = None, elevation: Optional[float] = None) -> None:
        """
        Rotates rotator.
        :param azimuth: Azimuth in degrees.
        :param elevation: Elevation in degrees.
        :return:
        """
        json_request = {}
        if azimuth is not None:
            json_request['azimuth'] = azimuth
        if elevation is not None:
            json_request['elevation'] = elevation
        request(self._publisher, self._ROTOR_CTL_TOPIC, json.dumps(json_request))
