import zmq
from typing import NamedTuple, Optional
from enum import Enum
import json

from zmq_util import request_response, request


class RelayState(Enum):
    """
    Relay state.
    """
    Off = False
    On = True


class RelaysState(NamedTuple):
    """
    Relays state.
    """
    rotator_relay: RelayState
    vhf_relay: RelayState
    uhf_relay: RelayState


class Relays:
    """
    Class for controlling relays.
    """
    _GET_RELAYS_TOPIC = 'get_relays'
    _RELAYS_STATE_TOPIC = 'relays_state'
    _SET_RELAYS_TOPIC = 'set_relays'

    def __init__(self, publisher: zmq.Socket, subscriber: zmq.Socket, poller: zmq.Poller, timeout: int) -> None:
        """
        Constructor.
        :param publisher: ZMQ publisher socket.
        :param subscriber: ZMQ subscriber socket.
        :param poller: ZMQ poller.
        :param timeout: Timeout in milliseconds.
        """
        self._publisher = publisher
        self._subscriber = subscriber
        self._poller = poller
        self._timeout = timeout

    @staticmethod
    def _parse_get_state(message: str) -> RelaysState:
        payload = json.loads(message)
        return RelaysState(
            rotator_relay=(RelayState.On if payload['rotator'] else RelayState.Off),
            vhf_relay=(RelayState.On if payload['filter2m'] else RelayState.Off),
            uhf_relay=(RelayState.On if payload['filter70cm'] else RelayState.Off),
        )

    def get_state(self) -> RelaysState:
        """
        Returns relays state.
        :return: Relays state.
        """
        return self._parse_get_state(request_response(
            subscriber=self._subscriber,
            publisher=self._publisher,
            poller=self._poller,
            request_topic=self._GET_RELAYS_TOPIC,
            response_topic=self._RELAYS_STATE_TOPIC,
            timeout=self._timeout
        ))

    def set_state(
            self,
            rotator: Optional[RelayState] = None,
            vhf: Optional[RelayState] = None,
            uhf: Optional[RelayState] = None) -> None:
        """
        Sets relays state.
        :param rotator: Optional rotator relay state.
        :param vhf: Optional VHF relay state.
        :param uhf: Optional UHF relay state.
        """
        result = {}
        if rotator is not None:
            result['rotator'] = rotator.value
        if vhf is not None:
            result['filter2m'] = vhf.value
        if uhf is not None:
            result['filter70cm'] = uhf.value
        request(self._publisher, self._SET_RELAYS_TOPIC, json.dumps(result))

    def all_on(self) -> None:
        """
        Turns all relays on.
        """
        self.set_state(RelayState.On, RelayState.On, RelayState.On)

    def all_off(self) -> None:
        """
        Turns all relays off.
        """
        self.set_state(RelayState.Off, RelayState.Off, RelayState.Off)
