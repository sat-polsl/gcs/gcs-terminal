import zmq
from gcs.relays import Relays
from gcs.frames_listener import FramesListener
from gcs.rotator import Rotator


class GCS:
    """
    GCS composition root.
    """
    def __init__(self, proxy_address: str, publisher_port: str, subscriber_port: str):
        """
        Constructor.
        :param proxy_address: Proxy address.
        :param publisher_port: Publisher port.
        :param subscriber_port: Subscriber port.
        """
        self._ctx = zmq.Context()
        self._publisher = self._ctx.socket(zmq.PUB)
        self._publisher.connect('tcp://{}:{}'.format(proxy_address, publisher_port))
        self._subscriber = self._ctx.socket(zmq.SUB)
        self._subscriber.connect('tcp://{}:{}'.format(proxy_address, subscriber_port))
        self._poller = zmq.Poller()
        self._poller.register(self._subscriber, zmq.POLLIN)

        self._relays = Relays(self._publisher, self._subscriber, self._poller, 1000)
        self._frames_listener = FramesListener(proxy_address, subscriber_port)
        self._rotator = Rotator(self._publisher, self._subscriber, self._poller, 1000)

    @property
    def relays(self) -> Relays:
        """
        Returns relays object.
        :return: Relays.
        """
        return self._relays

    @property
    def frames_listener(self) -> FramesListener:
        """
        Returns frames listener object.
        :return: Frames listener.
        """
        return self._frames_listener

    @property
    def rotator(self) -> Rotator:
        """
        Returns rotator object.
        :return: Rotator.
        """
        return self._rotator
