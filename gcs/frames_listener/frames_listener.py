import sys
import zmq
import logging
from datetime import datetime


class FramesListener:
    """
    Class for logging frames.
    """

    def __init__(self, proxy_address: str, subscriber_port: str) -> None:
        """
        Constructor.
        :param proxy_address: Proxy address.
        :param subscriber_port: Subscriber port.
        """
        self._proxy_address = proxy_address
        self._subscriber_port = subscriber_port

    def terminal(self) -> None:
        """
        Logs frames on GCS network to terminal.
        """
        socket = zmq.Socket(zmq.Context(), zmq.SUB)
        socket.connect('tcp://{}:{}'.format(self._proxy_address, self._subscriber_port))
        socket.subscribe('')

        while True:
            try:
                message = socket.recv_multipart()
                now = datetime.now().isoformat(sep=' ', timespec='milliseconds')
                print('[{}] [{}] {}'.format(now, message[0].decode('utf-8'), message[1].decode('utf-8')))
            except KeyboardInterrupt:
                break
