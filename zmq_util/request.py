import zmq


def request(publisher: zmq.Socket, request_topic: str, request: str = '{}') -> None:
    """
    ZMQ request wrapper.
    :param publisher: ZMQ publisher socket.
    :param request_topic: Request topic.
    :param request: Optional request.
    """
    publisher.send_string(request_topic, zmq.SNDMORE)
    publisher.send_string(request)
