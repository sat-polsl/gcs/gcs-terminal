import zmq


def request_response(
        publisher: zmq.Socket,
        subscriber: zmq.Socket,
        poller: zmq.Poller,
        request_topic: str,
        response_topic: str,
        timeout: int,
        request: str = '{}') -> str:
    """
    ZMQ request/response wrapper.
    :param publisher: ZMQ publisher socket.
    :param subscriber: ZMQ subscriber socket.
    :param poller: ZMQ poller.
    :param request_topic: ZMQ request topic.
    :param response_topic: ZMQ response topic.
    :param timeout: Timeout in milliseconds.
    :param request: Optional request.
    :return: Response.
    """
    subscriber.subscribe(response_topic)
    publisher.send_string(request_topic, zmq.SNDMORE)
    publisher.send_string(request)
    socks = dict(poller.poll(timeout))
    subscriber.unsubscribe(response_topic)
    if subscriber in socks:
        msg = subscriber.recv_multipart(zmq.NOBLOCK)
        if msg[0].decode('utf-8') == response_topic:
            return msg[1].decode('utf-8')
        else:
            raise Exception('[request_response] Unexpected topic: {}'.format(msg[0]))
    else:
        raise Exception('[request_response] Timeout')
