# GCS terminal

## Usage

1. `python -m venv venv`
2. Activate the virtual environment
3. `pip install -r requirements.txt`
4. `python terminal.py`
